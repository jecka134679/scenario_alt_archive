﻿label alt_day1_me_7dl_start:
    call alt_day1_me_7dl_vars
    $ routetag = "final"
    $ persistent.sprite_time = "day"
    $ day_time()
    $ alt_chapter(1, u"Пробуждение")
    call alt_day1_me_7dl_bus_start
    pause(1)
    $ alt_chapter(1, u"Экскурсия")
    call alt_day1_me_7dl_meeting
    pause(1)
    $ alt_save_name(1, u"Флэшбэк 88")
    call alt_day1_me_7dl_memory1
    pause(1)
    $ alt_save_name(1, u"Роковая встреча")
    call alt_day1_me_7dl_lena
    pause(1)
    $ persistent.sprite_time = "sunset"
    $ sunset_time()
    $ alt_chapter(1, u"Ужин")
    call alt_day1_me_7dl_supper
    pause(1)
    $ alt_save_name(1, u"Музыкальный клуб")
    call alt_day1_me_7dl_music_club
    pause(1)
    $ alt_save_name(1, u"Флэшбэк 88, часть 2")
    call alt_day1_me_7dl_memory2
    pause(1)
    $ alt_save_name(1, u"Отбой")
    call alt_day1_me_7dl_sleep

    pause(1)
    show spill_red with dspr
    $ renpy.pause(2, hard=True)
    show spill_gray with dspr
    $ renpy.pause(2, hard=True)
    show alt_credits timeskip_dev at truecenter with dissolve2
    $ renpy.pause(4, hard=True)
    with dissolve2
    return